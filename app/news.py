"""
News scrapers.
"""
from abc import abstractmethod
from dataclasses import dataclass
from typing import List

import requests
from bs4 import BeautifulSoup


@dataclass
class Article:
    """Represents an article from a news server."""
    header: str
    url: str


class NewsScraper:
    @abstractmethod
    def get_headers(self) -> List[Article]:
        """Returns a list of articles from the news server."""
        pass


class ResonancedailyScraper(NewsScraper):
    def get_headers(self) -> List[Article]:
        """
        Scrapes articles from Resonancedaily.
        """
        url = 'https://www.resonancedaily.com/'
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        articles = []
        for article in soup.find_all('div', class_='rb'):
            header = article.find('a').text
            url = "https://www.resonancedaily.com/" + article.find('a')['href']
            articles.append(Article(header, url))
        return articles


class GeorgiatodayScraper(NewsScraper):
    def get_headers(self) -> List[Article]:
        """
        Scrapes articles from Georgiatoday.
        """
        url = 'https://georgiatoday.ge/category/news/'
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        articles = []
        for article in soup.find_all('article', class_='jeg_post'):
            header = article.find('h3', class_='jeg_post_title').text.strip()
            url = article.find('a')['href']
            articles.append(Article(header, url))

        return articles


class BbcScraper(NewsScraper):
    def get_headers(self) -> List[Article]:
        """
        Scrapes articles from BBC.
        """
        url = 'https://www.bbc.com/news'
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        articles = []

        for article in soup.find_all("div", {"data-testid": "edinburgh-card"}):
            if article.find('a')['href'] is None:
                continue
            header = article.find("h2", {"data-testid": "card-headline"}).text
            url = 'https://www.bbc.com' + article.find('a')['href']
            articles.append(Article(header, url))

        return articles


class IdnesScraper(NewsScraper):
    def get_headers(self) -> List[Article]:
        """
        Scrapes articles from idnes.cz.
        """
        url = 'https://www.idnes.cz/'
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        articles = []
        for article in soup.find_all('div', class_='art'):
            if article.find('h3') is None:
                continue
            header = article.find('h3').text.strip()
            url = article.find('a')['href']
            articles.append(Article(header, url))

        return articles

