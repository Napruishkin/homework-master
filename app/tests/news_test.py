import re

from app.news import ResonancedailyScraper, GeorgiatodayScraper, BbcScraper, NewsScraper, IdnesScraper


def check_provider(provider: NewsScraper):
    articles = provider.get_headers()
    print(articles)
    assert len(articles) > 0
    for a in articles:
        assert len(a.header) > 0
        assert a.header.strip() == a.header
        assert re.fullmatch(r'http(s)?://.*', a.url)


def test_resonancedaily():
    check_provider(ResonancedailyScraper())


def test_georgiatoday():
    check_provider(GeorgiatodayScraper())


def test_bbc():
    check_provider(BbcScraper())


def test_idnes():
    check_provider(IdnesScraper())
