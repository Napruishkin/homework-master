"""
Web scraping service.
"""
import logging

from app.news import ResonancedailyScraper, GeorgiatodayScraper, BbcScraper, IdnesScraper
from app.service import save_article_if_new

logger = logging.getLogger(__name__)
SCRAPERS = [ResonancedailyScraper(), GeorgiatodayScraper(), BbcScraper(), IdnesScraper()]


def scrape_news():
    """Gets articles from news servers and saves new ones into our DB.
    If any scraper fails, it must be logged but continue in operation.
    """
    for scraper in SCRAPERS:
        logger.info(f"Scraping news using {type(scraper).__name__}")
        try:
            articles = scraper.get_headers()
            for article in articles:
                save_article_if_new(article)
        except Exception as e:
            logger.exception(f"Error while scraping {type(scraper).__name__}:")


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='{asctime} {levelname:<8} {name}:{module}:{lineno} - {message}',
                        style='{')
    scrape_news()
    import time
    import schedule

    schedule.every(1).minutes.do(scrape_news)

    while True:
        schedule.run_pending()
        time.sleep(1)
