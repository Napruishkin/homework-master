"""
Service layer.
"""
import logging
from typing import List

from sqlalchemy import desc, or_

from app import db
from app.model import Article

logger = logging.getLogger(__name__)


def get_articles_with_keywords(keywords: List[str]) -> List[Article]:
    """
    Returns all articles from DB where at least one of given keywords is in article's header.
    The newest articles will be first. If no keywords were given, should return an empty list.
    """
    if not keywords:
        return []  # Return an empty list if no keywords were given

    query = db.session.query(Article).filter(or_(*[Article.header.contains(keyword) for keyword in keywords]))
    query = query.order_by(Article.timestamp.desc())

    return query.all()


def save_article_if_new(a: Article) -> None:
    """Saves an article, if it is not in our DB already. Existence is checked by URL of the article."""
    existing_article = db.session.query(Article).filter_by(url=a.url).first()
    if existing_article is None:
        article = Article(url=a.url, header=a.header)
        db.session.add(article)
        db.session.commit()
