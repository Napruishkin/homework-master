"""
DB definitions.
"""

from typing import Union

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, scoped_session, sessionmaker

engine = create_engine('postgresql://postgres:postgres@localhost:5432/postgres')

session: Union[Session, scoped_session] = scoped_session(sessionmaker(bind=engine))


def create_empty_db():
    from app.model import Base
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
